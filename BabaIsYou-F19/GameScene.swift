//
//  GameScene.swift
//  BabaIsYou-F19
//
//  Created by Parrot on 2019-10-17.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {

    var baba = SKSpriteNode()
    var wall = SKSpriteNode()
    var wallBlock = SKSpriteNode()

    var flag = SKSpriteNode()
    var flagBlock = SKSpriteNode()

    var isBlock = SKSpriteNode()
    var stopBlock = SKSpriteNode()
    var winBlock = SKSpriteNode()
    
    var message = SKLabelNode()

    var wallIsActive:Bool = false

    let PLAYER_DISPLACEMENT:CGFloat = 20

    
    override func didMove(to view: SKView) {
        
        self.message = self.childNode(withName: "message") as! SKLabelNode
        
        self.baba = self.childNode(withName: "player") as! SKSpriteNode
        self.baba.physicsBody?.categoryBitMask = PhysicsCategory.babaCategory
        self.baba.physicsBody?.collisionBitMask = PhysicsCategory.blockCategory
        self.baba.physicsBody?.contactTestBitMask = PhysicsCategory.wallCategory
        
        self.wall = self.childNode(withName: "wall") as! SKSpriteNode
        self.wall.physicsBody?.categoryBitMask = PhysicsCategory.wallCategory
        self.wall.physicsBody?.collisionBitMask = PhysicsCategory.none
        self.wall.physicsBody?.contactTestBitMask = PhysicsCategory.babaCategory | PhysicsCategory.blockCategory
        
        self.wallBlock = self.childNode(withName: "wallblock") as! SKSpriteNode
        self.wallBlock.physicsBody?.categoryBitMask = PhysicsCategory.blockCategory
        self.wallBlock.physicsBody?.collisionBitMask = PhysicsCategory.babaCategory | PhysicsCategory.blockCategory
        self.wallBlock.physicsBody?.contactTestBitMask = PhysicsCategory.blockCategory | PhysicsCategory.babaCategory
        
        self.flagBlock = self.childNode(withName: "flagblock") as! SKSpriteNode
        self.flagBlock.physicsBody?.categoryBitMask = PhysicsCategory.blockCategory
        self.flagBlock.physicsBody?.collisionBitMask = PhysicsCategory.babaCategory | PhysicsCategory.blockCategory
        self.flagBlock.physicsBody?.contactTestBitMask = PhysicsCategory.blockCategory | PhysicsCategory.babaCategory
        
        self.flag = self.childNode(withName: "flag") as! SKSpriteNode
        self.flag.physicsBody?.categoryBitMask = PhysicsCategory.flagCategory
        self.flag.physicsBody?.collisionBitMask = PhysicsCategory.none
        self.baba.physicsBody?.contactTestBitMask = PhysicsCategory.babaCategory

        self.isBlock = self.childNode(withName: "isblock") as! SKSpriteNode
        self.isBlock.physicsBody?.categoryBitMask = PhysicsCategory.blockCategory
        self.isBlock.physicsBody?.collisionBitMask = PhysicsCategory.none
        self.isBlock.physicsBody?.contactTestBitMask = PhysicsCategory.blockCategory | PhysicsCategory.babaCategory

        self.stopBlock = self.childNode(withName: "stopblock") as! SKSpriteNode
        self.stopBlock.physicsBody?.categoryBitMask = PhysicsCategory.blockCategory
        self.stopBlock.physicsBody?.collisionBitMask = PhysicsCategory.babaCategory | PhysicsCategory.blockCategory
        self.stopBlock.physicsBody?.contactTestBitMask = PhysicsCategory.blockCategory | PhysicsCategory.babaCategory

        self.winBlock = self.childNode(withName: "winblock") as! SKSpriteNode
        self.winBlock.physicsBody?.categoryBitMask = PhysicsCategory.blockCategory
        self.winBlock.physicsBody?.collisionBitMask = PhysicsCategory.babaCategory | PhysicsCategory.blockCategory
        self.winBlock.physicsBody?.contactTestBitMask = PhysicsCategory.blockCategory | PhysicsCategory.babaCategory
        
        self.physicsWorld.contactDelegate = self
        
        if (self.wallBlock.position.x + self.wallBlock.size.width == self.isBlock.position.x) &&
            (self.isBlock.position.x + self.isBlock.size.width == self.stopBlock.position.x) {
            wallIsActive = true
        }
    }
   
    func didBegin(_ contact: SKPhysicsContact) {
        let nodeA = contact.bodyA.node
        let nodeB = contact.bodyB.node
        if (nodeA == nil || nodeB == nil) {
                   return
               }
               
        if (nodeA!.name == "baba" && nodeB!.name == "wall") {
            // player die
            if wallIsActive {
                contact.bodyB.categoryBitMask = PhysicsCategory.none
            }
        }
        if (nodeA!.name == "baba" && nodeB!.name == "flag") {
            // player die
            if isRuleActive(node1: self.flagBlock, node2: self.winBlock) {
                message.text = "You win"
                
            }
        }
        
        let categoryA = contact.bodyA.collisionBitMask
        let categoryB = contact.bodyB.collisionBitMask
        
        if categoryA == PhysicsCategory.blockCategory && categoryB == PhysicsCategory.blockCategory {
           
        }

    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
            if (touch == nil) {
                return
            }
            let touchLocation = touch!.location(in: self)
           
            let touchedNode = atPoint(touchLocation).name
        
            if (touchedNode == "upButton") {
                self.baba.position.y = self.baba.position.y + PLAYER_DISPLACEMENT
            }
            else if (touchedNode == "downButton") {
                 self.baba.position.y = self.baba.position.y - PLAYER_DISPLACEMENT
            }
            else if (touchedNode == "leftButton") {
                 self.baba.position.x = self.baba.position.x - PLAYER_DISPLACEMENT
            }
            else if (touchedNode == "rightButton") {
                 self.baba.position.x = self.baba.position.x + PLAYER_DISPLACEMENT
            }
    }
    
    
    struct PhysicsCategory {
           static let none: UInt32 = 0
           static let babaCategory: UInt32 = 0b1
           static let wallCategory: UInt32 = 0b10
           static let blockCategory: UInt32 = 0b100
           static let flagCategory: UInt32 = 0b1000

        }
    
    func isRuleActive(node1: SKSpriteNode, node2: SKSpriteNode) -> Bool{
        var ruleActive: Bool = false
        if (node1.position.x + node1.size.width == self.isBlock.position.x) &&
            (self.isBlock.position.x + self.isBlock.size.width == node2.position.x) {
            ruleActive = true
        }else {
            ruleActive = false
        }
        return ruleActive
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
}
